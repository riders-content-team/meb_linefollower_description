#include <meb_linefollower_description/RobotControlPlugin.hpp>
#include <stdlib.h>
#include <iostream>
namespace gazebo
{
    enum {
        RIGHT,
        LEFT,
    };


    RobotControlPlugin::RobotControlPlugin() {}


    RobotControlPlugin::~RobotControlPlugin()
    {
        RobotControlPlugin::Destroy();
    }


    void RobotControlPlugin::Load (physics::ModelPtr _model, sdf::ElementPtr _sdf)
    {
        this->model = _model;
        this->world = this->model->GetWorld();
        /* this->world->ResetEntities(gazebo::physics::Base::MODEL); */
        /* this->world->Reset(); */

        this->robot_namespace = this->model->GetName();
        

        //Get wheel joints
        if (!_sdf->HasElement("left_joint_name"))
        {
            this->left_joint_name = "joint_wheel_left";
        } else {
            this->left_joint_name = _sdf->Get<std::string>("left_joint_name");
        }

        if (!_sdf->HasElement("right_joint_name"))
        {
            this->right_joint_name = "joint_wheel_right";
        } else {
            this->right_joint_name = _sdf->Get<std::string>("right_joint_name");
        }
        
        if (!_sdf->HasElement("torque"))
        {
            this->torque = 5;
        } else {
            this->torque = _sdf->Get<double>("torque");
        }

        if (!_sdf->HasElement("updateRate"))
        {
            this->update_rate = 100.0;
        } else {
            this->update_rate = _sdf->Get<double>("updateRate");
        }

        // Initialize update rate stuff
        if (this->update_rate > 0.0) {
            this->update_period = 1.0 / this->update_rate;
        } else {
            this->update_period = 0.0;
        }

        this->last_update_time = this->world->SimTime().Double();

        this->motor_speeds.resize(2);
        this->motor_speeds[LEFT] = 0;
        this->motor_speeds[RIGHT] = 0;
        
        this->joints.resize(2);
        this->joints[LEFT] = this->model->GetJoint("joint_wheel_left");
        this->joints[RIGHT] = this->model->GetJoint("joint_wheel_right");

        if (!(this->joints[LEFT] && this->joints[RIGHT])) {
            char error[200];
            snprintf(
                error,
                200,
                "RobotControlPlugin (ns = %s) couldn't get one of the joints",
                this->robot_namespace.c_str()
            );
            gzthrow(error);
        }

        this->joints[LEFT]->SetParam("fmax", 0, this->torque);
        this->joints[RIGHT]->SetParam("fmax", 0, this->torque);

        // Initialize ros, if it has not already bee initialized.
        if (!ros::isInitialized())
        {
            int argc = 0;
            char **argv = NULL;
            ros::init(
                argc,
                argv,
                this->robot_namespace,
                ros::init_options::NoSigintHandler
            );
        }

        // Create our ROS node. This acts in a similar manner to
        // the Gazebo node
        this->rosNode.reset(new ros::NodeHandle(this->robot_namespace));

        this->left_command_topic = "/" + this->robot_namespace + "/" + this->left_joint_name + "/set_speed";
        this->right_command_topic = "/" + this->robot_namespace + "/" + this->right_joint_name + "/set_speed";
        
        ros::SubscribeOptions left_so = ros::SubscribeOptions::create<std_msgs::Float64>(
            this->left_command_topic,
            1,
            std::bind(&RobotControlPlugin::LeftSpeedCallback, this, std::placeholders::_1),
            ros::VoidConstPtr(),
            NULL
        );

        ros::SubscribeOptions right_so = ros::SubscribeOptions::create<std_msgs::Float64>(
            this->right_command_topic,
            1,
            std::bind(&RobotControlPlugin::RightSpeedCallback, this, std::placeholders::_1),
            ros::VoidConstPtr(),
            NULL
        );
        
        this->subscribers.resize(2);
        this->subscribers[LEFT] = this->rosNode->subscribe(left_so);
        this->subscribers[RIGHT] = this->rosNode->subscribe(right_so);


        this->worldConnection = event::Events::ConnectWorldUpdateBegin(
            std::bind(&RobotControlPlugin::Update, this));

    }
    
    void RobotControlPlugin::Update()
    {
        double current_time = this->world->SimTime().Double();
        double seconds_since_last_update = current_time - this->last_update_time;

        if (seconds_since_last_update > this->update_period)
        {
            this->joints[LEFT]->SetParam("vel", 0, this->motor_speeds[LEFT]);
            this->joints[RIGHT]->SetParam("vel", 0, this->motor_speeds[RIGHT]);
            
            this->last_update_time = this->world->SimTime().Double();
        }

    }


    void RobotControlPlugin::LeftSpeedCallback(const std_msgs::Float64::ConstPtr& msg)
    {
        this->motor_speeds[LEFT] = msg->data;
        //std::cout << msg->data << '\n';
    }


    void RobotControlPlugin::RightSpeedCallback(const std_msgs::Float64::ConstPtr& msg)
    {
        this->motor_speeds[RIGHT] = msg->data;
        //std::cout << msg->data << '\n';
    }

    
    void RobotControlPlugin::Reset()
    {

    }


    void RobotControlPlugin::Destroy()
    {

    }


    GZ_REGISTER_MODEL_PLUGIN(RobotControlPlugin)
}

