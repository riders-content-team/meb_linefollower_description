#ifndef ROBOT_CONTROL_PLUGIN_HH
#define ROBOT_CONTROL_PLUGIN_HH

#include <algorithm>
#include "gazebo/common/common.hh"
#include "ros/ros.h"
#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/MultiRayShape.hh"
#include "gazebo/physics/World.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/physics/Joint.hh"
#include "gazebo/physics/PhysicsIface.hh"
#include <std_msgs/Float64.h>

namespace gazebo {
    class Joint;
    class Entity;

    class RobotControlPlugin : public ModelPlugin {

        public:
            RobotControlPlugin();
            ~RobotControlPlugin();
            void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);
            void Reset();

        protected:
            virtual void Update();
            virtual void Destroy();
            virtual void LeftSpeedCallback(const std_msgs::Float64::ConstPtr& msg);
            virtual void RightSpeedCallback(const std_msgs::Float64::ConstPtr& msg);
            std::unique_ptr<ros::NodeHandle> rosNode;
            /* ros::ServiceServer control_service; */

        private:
            physics::ModelPtr model;
            physics::WorldPtr world;

            event::ConnectionPtr worldConnection;

            std::vector<physics::JointPtr> joints;
            std::vector<double> motor_speeds;
            std::vector<ros::Subscriber> subscribers;

            std::string left_joint_name, right_joint_name, left_command_topic, right_command_topic, robot_namespace;
            double torque, update_rate, update_period, last_update_time;
    };


}
#endif

