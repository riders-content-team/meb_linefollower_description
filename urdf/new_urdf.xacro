<?xml version="1.0" ?>
<robot name="rosrider" xmlns:xacro="http://ros.org/wiki/xacro">

  <xacro:property name="mass_scale" value="10000.0"/>

  <xacro:property name="body_shape" value="box"/>
  <xacro:property name="body_shift_forward" value="-0.1"/>
  <xacro:property name="body_shift_up" value="0.05"/>
  <xacro:property name="body_radius" value="0.04"/>
  <xacro:property name="body_length" value="0.25"/>
  <xacro:property name="body_width" value="0.17"/>
  <xacro:property name="body_height" value="0.05"/>

  <xacro:property name="front_axle_length" value="0.25"/>
  <xacro:property name="front_wheel_radius" value="0.06"/>
  <xacro:property name="front_wheel_thickness" value="0.03"/>

  <xacro:property name="scoop_length" value="0.25"/>
  <xacro:property name="scoop_side_distance" value="0.17"/>
  <xacro:property name="scoop_radius" value="0.01"/>

  <xacro:property name="caster_radius" value="0.035"/>
  <xacro:property name="caster_shift_backward" value="0.22"/>

  <xacro:property name="body_color" value="Orange"/>
  <xacro:property name="arm_color" value="Orange"/>
  <xacro:property name="wheel_color" value="Orange"/>
  <xacro:property name="wheel_axis_color" value="Black"/>


  <xacro:property name="pi" value="3.141592654"/>

  <xacro:property name="front_wheel_mass" value="${mass_scale*front_wheel_thickness*pi*front_wheel_radius*front_wheel_radius}"/>
  <xacro:property name="front_axis_radius" value="0.01"/>
  <xacro:property name="front_axis_mass" value="${mass_scale*front_axle_length*pi*front_axis_radius*front_axis_radius}"/>
  <xacro:property name="scoop_mass" value="${mass_scale*scoop_length*pi*scoop_radius*scoop_radius}"/>


  <xacro:property name="caster_mass" value="${(mass_scale*(4.0/3.0)*pi*caster_radius*caster_radius*caster_radius)*0.1}"/>
  <xacro:property name="caster_inertia" value="${caster_mass*(2.0/5.0)*caster_radius*caster_radius}"/>

  <xacro:property name="body_mass" value="${mass_scale*body_length*body_width*body_height}"/>
  <xacro:property name="body_Ixx" value="${(body_mass/12.0)*((body_width*body_width)+(body_height*body_height))}"/>
  <xacro:property name="body_Iyy" value="${(body_mass/12.0)*((body_length*body_length)+(body_height*body_height))}"/>
  <xacro:property name="body_Izz" value="${(body_mass/12.0)*((body_length*body_length)+(body_width*body_width))}"/>



  <gazebo reference="base_link">
    <material>Gazebo/${body_color}</material>
  </gazebo>
  <gazebo reference="wheel_left_link">
    <material>Gazebo/${wheel_color}</material>
  </gazebo>
  <gazebo reference="wheel_right_link">
    <material>Gazebo/${wheel_color}</material>
  </gazebo>
  <gazebo reference="right_scoop_link">
    <material>Gazebo/${arm_color}</material>
  </gazebo>
  <gazebo reference="left_scoop_link">
    <material>Gazebo/${arm_color}</material>
  </gazebo>
  <gazebo reference="wheel_axis_link">
    <material>Gazebo/${wheel_axis_color}</material>
  </gazebo>

  <link name="base_footprint"/>

  <joint name="base_joint" type="fixed">
    <parent link="base_footprint"/>
    <child link="base_link"/>
    <origin xyz="0.0 0.0 0.03" rpy="0 0 0"/>
  </joint>

  <link name="base_link">
    <visual>
      <origin xyz="${body_shift_forward} 0 ${body_shift_up}" rpy="0 0 0"/>
      <geometry>
        <xacro:if value="${body_shape == 'sphere'}">
          <sphere radius="${body_radius}"/>
        </xacro:if>
        <xacro:if value="${body_shape == 'box'}">
          <box size="${body_length} ${body_width} ${body_height}"/>
        </xacro:if>
      </geometry>
      
    </visual>
    <collision>
      <origin xyz="${body_shift_forward} 0 ${body_shift_up}" rpy="0 0 0"/>
      <geometry>
        <xacro:if value="${body_shape == 'sphere'}">
          <sphere radius="${body_radius}"/>
        </xacro:if>
        <xacro:if value="${body_shape == 'box'}">
          <box size="${body_length} ${body_width} ${body_height}"/>
        </xacro:if>
      </geometry>
    </collision>
    <inertial>
      <origin xyz="${body_shift_forward} 0 ${body_shift_up}" rpy="0 0 0"/>
      <mass value="${body_mass}"/>
      <inertia ixx="${body_Ixx}" ixy="0.0" ixz="0.0"
               iyy="${body_Iyy}" iyz="0.0"
               izz="${body_Izz}" />
    </inertial>
  </link>

  <joint name="wheel_left_joint" type="continuous">
    <parent link="base_link"/>
    <child link="wheel_left_link"/>
    <origin xyz="-0.02 ${front_axle_length/2.0} 0.0" rpy="-1.570796 0 0"/>
    <axis xyz="0 0 1"/>
  </joint>

  <link name="wheel_left_link">
    <visual>
      <origin xyz="0 0 0" rpy="1.570796 0 0"/>
      <geometry>
        <!-- <mesh filename="package://rosrider_description/meshes/wheel_left.dae" scale="${front_wheel_radius*0.001/0.03} ${front_wheel_thickness*0.001/0.01} ${front_wheel_radius*0.001/0.03}"/> -->
        <cylinder length="${front_wheel_thickness}" radius="${front_wheel_radius}"/>
      </geometry>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <cylinder length="${front_wheel_thickness}" radius="${front_wheel_radius}"/>
      </geometry>
    </collision>
    <inertial>
      <mass value="${front_wheel_mass}" />
      <inertia ixx="${(front_wheel_mass/12)*((3*front_wheel_radius*front_wheel_radius)+(front_wheel_thickness*front_wheel_thickness))}" ixy="0.0" ixz="0.0"
               iyy="${(front_wheel_mass/12)*((3*front_wheel_radius*front_wheel_radius)+(front_wheel_thickness*front_wheel_thickness))}" iyz="0.0"
               izz="${0.5*front_wheel_mass*front_wheel_radius*front_wheel_radius}" />
    </inertial>
  </link>

  <joint name="wheel_right_joint" type="continuous">
    <parent link="base_link"/>
    <child link="wheel_right_link"/>
    <origin xyz="-0.02 -${front_axle_length/2.0} 0.0" rpy="-1.570796 0 0"/>
    <axis xyz="0 0 1"/>
  </joint>

  <joint name="right_scoop_joint" type="continuous">
    <parent link="base_link"/>
    <child link="right_scoop_link"/>
    <origin xyz="0.18 ${scoop_side_distance} 0.0" rpy="-1.570796 0 1.570796"/>
    <axis xyz="0 0 1"/>
  </joint>

  <link name="right_scoop_link">
    <visual>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <cylinder length="${scoop_length}" radius="${scoop_radius}"/>
      </geometry>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <cylinder length="${scoop_length}" radius="${scoop_radius}"/>
      </geometry>
    </collision>
    <inertial>
      <mass value="${scoop_mass}" />
      <inertia ixx="${(scoop_mass/12)*((3*scoop_radius*scoop_radius)+(scoop_length*scoop_length))}" ixy="0.0" ixz="0.0"
               iyy="${(scoop_mass/12)*((3*scoop_radius*scoop_radius)+(scoop_length*scoop_length))}" iyz="0.0"
               izz="${0.5*scoop_mass*scoop_radius*scoop_radius}" />
    </inertial>
  </link>


  <joint name="left_scoop_joint" type="continuous">
    <parent link="base_link"/>
    <child link="left_scoop_link"/>
    <origin xyz="0.18 ${-1.0*scoop_side_distance} 0.0" rpy="-1.570796 0 1.570796"/>
    <axis xyz="0 0 1"/>
  </joint>

  <link name="left_scoop_link">
    <visual>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <cylinder length="${scoop_length}" radius="${scoop_radius}"/>
      </geometry>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <cylinder length="${scoop_length}" radius="${scoop_radius}"/>
      </geometry>
    </collision>
    <inertial>
      <mass value="${scoop_mass}" />
      <inertia ixx="${(scoop_mass/12)*((3*scoop_radius*scoop_radius)+(scoop_length*scoop_length))}" ixy="0.0" ixz="0.0"
               iyy="${(scoop_mass/12)*((3*scoop_radius*scoop_radius)+(scoop_length*scoop_length))}" iyz="0.0"
               izz="${0.5*scoop_mass*scoop_radius*scoop_radius}" />
    </inertial>
  </link>

  <link name="wheel_right_link">
    <visual>
      <origin xyz="0 0 0" rpy="1.570796 0 0"/>
      <geometry>
        <!-- <mesh filename="package://rosrider_description/meshes/wheel_right.dae" scale="${front_wheel_radius*0.001/0.03} ${front_wheel_thickness*0.001/0.01} ${front_wheel_radius*0.001/0.03}"/> -->
        <cylinder length="${front_wheel_thickness}" radius="${front_wheel_radius}"/>
      </geometry>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <cylinder length="${front_wheel_thickness}" radius="${front_wheel_radius}"/>
      </geometry>
    </collision>
    <inertial>
      <mass value="${front_wheel_mass}" />
      <inertia ixx="${(front_wheel_mass/12)*((3*front_wheel_radius*front_wheel_radius)+(front_wheel_thickness*front_wheel_thickness))}" ixy="0.0" ixz="0.0"
               iyy="${(front_wheel_mass/12)*((3*front_wheel_radius*front_wheel_radius)+(front_wheel_thickness*front_wheel_thickness))}" iyz="0.0"
               izz="${0.5*front_wheel_mass*front_wheel_radius*front_wheel_radius}" />
    </inertial>
  </link>

  <joint name="wheel_axis_joint" type="continuous">
    <parent link="base_link"/>
    <child link="wheel_axis_link"/>
    <origin xyz="0.06 0.0 0.0" rpy="-1.570796 0 0"/>
    <axis xyz="0 0 1"/>
  </joint>

  <link name="wheel_axis_link">
    <visual>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <cylinder length="0.3" radius="${front_axis_radius}"/>
      </geometry>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <cylinder length="${scoop_side_distance}" radius="${front_axis_radius}"/>
      </geometry>
    </collision>
    <inertial>
      <mass value="${front_axis_mass}" />
      <inertia ixx="${0.25*front_axis_mass*front_axis_radius*front_axis_radius}" ixy="0.0" ixz="0.0"
               iyy="${0.25*front_axis_mass*front_axis_radius*front_axis_radius}" iyz="0.0"
               izz="${0.5*front_axis_mass*front_axis_radius*front_axis_radius}" />
    </inertial>
  </link>

  <joint name="caster_back_joint" type="fixed">
    <parent link="base_link"/>
    <child link="caster_back_link"/>
    <origin xyz="${caster_shift_backward*-1.0} 0 -0.020495" rpy="0 0 0"/>
  </joint>

  <link name="caster_back_link">
    <visual>
      <geometry>
        <sphere radius="${caster_radius}"/>
      </geometry>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0"/>
      <geometry>
        <sphere radius="${caster_radius}"/>
      </geometry>
    </collision>
    <inertial>
      <origin xyz="0 0 0" />
      <mass value="${caster_mass}" />
      <inertia ixx="${caster_inertia}" ixy="0.0" ixz="0.0"
               iyy="${caster_inertia}" iyz="0.0"
               izz="${caster_inertia}" />
    </inertial>
  </link>

  <joint name="camera_joint" type="fixed">
    <origin xyz="0.0 0 0.1" rpy="0 0.0 0"/>
    <parent link="base_link"/>
    <child link="camera_link"/>
  </joint>

  <link name="camera_link">
  </link>

  <joint name="camera_rgb_joint" type="fixed">
    <origin xyz="0 0 -0.08" rpy="0 0 0"/>
    <parent link="camera_link"/>
    <child link="camera_rgb_frame"/>
  </joint>
  <link name="camera_rgb_frame"/>

  <joint name="camera_rgb_optical_joint" type="fixed">
    <origin xyz="0 0 0" rpy="-1.570796 0 -1.570796"/>
    <parent link="camera_rgb_frame"/>
    <child link="camera_rgb_optical_frame"/>
  </joint>
  <link name="camera_rgb_optical_frame"/>

  <joint name="sensor_front_joint" type="fixed">
      <origin xyz="0.05893 -0.02 0.03577" rpy="0 0 0"/>
      <parent link="base_link"/>
      <child link="sensor_front_link"/>
  </joint>
  <link name="sensor_front_link"/>

</robot>






